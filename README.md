# Wikimedia Transparency Report

Blubber images for transparency report static html site

# Local development of static site

To run:
* ```gem install bundler```
* ```bundle install```
* ```bundle exec middleman```

To build:
* ```cd TransparencyReport```
* ```bundle exec middleman build```
* The generate the static version of the site in the /build folder
* Copy the files in the /build folder on a server to host


# Image build:

Mirror of https://gerrit.wikimedia.org/r/plugins/gitiles/wikimedia/TransparencyReport/

## Local image build

Build image locally:
```
DOCKER_BUILDKIT=1 docker build --target transparencyreport -f .pipeline/blubber.yaml .
```

Run image:
```
docker run -p 8080:8080 <image name>
```

Visit `http://127.0.0.1:8080/2014/`


## Publish new image version

To create a new image version merge your change into the master branch.

This triggers the publish-image pipeline. Image is available at `docker-registry.wikimedia.org/repos/sre/miscweb/transparencyreport:<timestamp>`
